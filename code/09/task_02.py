import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = ((cos(x))^2 + 1) / (cos(2*y) - 1)
Z2 = ctan(y/2 - x/3)
Z3 = |Z2 / Z1 - 5 / Z1| / (pi * y)
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d = math.cos(2 * y) - 1
        if math.cos(2 * y) - 1 != 0:
            z1 = (math.cos(x) ** 2 + 1) / d
            z2 = math.atan(math.tan(y / 2 - x / 3))
            if (math.cos(x) ** 2 + 1) / (math.cos(2 * y) - 1) != 0:
                if (math.pi * y) != 0:
                    z3 = abs((math.atan(math.tan(y / 2 - x / 3))) / ((math.cos(x) ** 2 + 1) / (math.cos(2 * y) - 1)) - 5 / ((math.cos(x) ** 2 + 1) / (math.cos(2 * y) - 1))) / (math.pi * y)
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Значення Z1 не може бути рівним нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
