import math
import sys

'''
Обчислити площу трикутника, якщо задано довжини сторін трикутника.
'''
class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
    @classmethod
    def get_sides(cls):
        try:
            a = float(input("Введіть довжину першої сторони: "))
            b = float(input("Введіть довжину другої сторони: "))
            c = float(input("Введіть довжину третьої сторони: "))
            return cls(a, b, c)
        except ValueError:
            print("Некоректні дані. Введіть числові значення для довжин сторін.")
            sys.exit(1)
    def triangle_check(self):
        return self.a + self.b > self.c and self.a + self.c > self.b and self.b + self.c > self.a

    def calc_area(self):
        p = (self.a + self.b + self.c) / 2
        return math.sqrt(p * (p - self.a) * (p - self.b) * (p - self.c))
def main():
        tl = Triangle.get_sides()
        if tl.triangle_check:
            area = tl.calc_area()
            print(f"Площа трикутника: {area}")
            sys.exit(0)
        else:
            print("Трикутник з такими сторонами не існує.")
            sys.exit(1)

if __name__ == "__main__":
    main()
