import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (tan(x) / (1 + ctan(y))
Z2 = cos(y + x) - x / sin (y + x)
Z3 = exp^(Z1+Z2) * (Z1 / Z2)
'''
def get_xy():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        return x, y
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)

def calc_z1(x,y):
        return math.tan(x) / (1 + math.atan(y))
def calc_z2(x,y):
    sin_yx = math.sin(y + x)
    if  sin_yx != 0:
        return math.cos(y + x) - x / sin_yx
    else:
        raise ValueError("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
def calc_z3(x,y):
    d = 1 + math.atan(y)
    return (math.exp(math.tan(x) / d) + (math.cos(y + x) - x)) * ((math.tan(x) / 1 + math.atan(y)) / math.cos(y + x) - x / math.sin(y + x))

def main():
    x,y = get_xy()
    z1 = calc_z1(x,y)
    z2 = calc_z2(x, y)
    z3 = calc_z3(x, y)
    print(f"Z1 = {z1}")
    print(f"Z2 = {z2}")
    print(f"Z3 = {z3}")
    sys.exit(0)

if __name__ == "__main__":
    main()
